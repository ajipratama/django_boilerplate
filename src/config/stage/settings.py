# Allowed Host
ALLOWED_HOSTS = ['']

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '#!jp&=h2+lzkpd)tsb8)zh#afp^ky-((=e*7z@f(4##+-d1i-0'

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

# For Postgresql
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': '<DATABASE_NAME>',
        'USER': '<USERNAME>',
        'HOST': '<HOST>',
        'PORT': 5432,
    }
}

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True
