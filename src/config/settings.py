import os
import sys
import socket

# Hostname
HOSTNAME = socket.gethostname()
PRODUCTION_SERVERS = ['livehost-hostaname']
STAGING_SERVERS = ['staging-hostname']

LIVEHOST = HOSTNAME in PRODUCTION_SERVERS
STAGEHOST = HOSTNAME in STAGING_SERVERS 

LOCALHOST = not LIVEHOST and not STAGEHOST

from .settings_apps import *

if LIVEHOST:
    from .live.settings import *    
elif STAGEHOST:
    from .stage.settings import *    
else:
    from .local.settings import *
