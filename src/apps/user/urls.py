from django.urls import include, path

from .views import UserCreate, UserDetail


urlpatterns = [
    path('<int:pk>/', UserDetail.as_view(), name='user_detail'),
    path('registration/', UserCreate.as_view(), name='washer_create')
]