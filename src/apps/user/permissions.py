from rest_framework import permissions

from .models import User


class IsUser(permissions.BasePermission):    
      def has_permission(self, request, view):
           if request.user.pk == view.kwargs['pk']:
              return True
           return False
