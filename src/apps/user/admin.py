from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User as CustomUser
from .forms import UserCreationForm, UserChangeForm


@admin.register(CustomUser)
class CustomUserAdmin(UserAdmin):
    list_display = ['username', 'first_name', 'last_name', 'email', 'date_joined']
    search_fields = ['username', 'first_name', 'last_name', 'email']
    form = UserChangeForm
    add_form = UserCreationForm
    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        ('Personal Info', {'fields': ('username', 'first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_superuser', 'is_admin',
                                    'is_staff', 'is_active',
                                    'user_permissions')})
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2')
        }
        ),
        ('Personal Info', {'fields': ('username', 'first_name', 'last_name')}),
        ('Permissions', {'fields': ('is_superuser', 'is_admin',
                                    'is_staff', 'is_active',
                                    'user_permissions')})
    )
