from django.db import models
from django.contrib.auth.models import (BaseUserManager, AbstractBaseUser,
                                        PermissionsMixin)

from versatileimagefield.fields import VersatileImageField

from base.functions import upload_to
from base.models import BaseModel


class UserManager(BaseUserManager):
    def create_user(self, email, password, **kwargs):
        if not email:
            raise ValueError('User must have a valid email')
        if not password:
            raise ValueError('User must have a valid password')

        user = self.model(email=self.normalize_email(email))
        user.set_password(password)
        user.save()

        return user

    def create_superuser(self, email, password, **kwargs):
        user = self.create_user(email, password, **kwargs)
        user.is_superuser = True
        user.is_admin = True
        user.is_staff = True
        user.is_active = True
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(max_length=100, unique=True)
    username = models.CharField(max_length=50, null=True, blank=True, unique=True)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50, null=True, blank=True)

    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    date_joined = models.DateTimeField(auto_now_add=True)

    objects = UserManager()

    USERNAME_FIELD = 'email'

    def is_washer(self):
        return self.status == 1

    def is_customer(self):
        return self.status == 2

    def save(self, *args, **kwargs):
        try:
            if not self.username:
                username = self.first_name + self.last_name[0:1]
                counter = 1
                while User.objects.filter(username=username):
                    username = self.first_name + self.last_name[0:counter]
                    counter += 1
                self.username = username.lower()
        except:
            self.username = self.first_name.lower() if self.username else self.email.lower().split('@')[0]

        super(User, self).save(*args, **kwargs)
