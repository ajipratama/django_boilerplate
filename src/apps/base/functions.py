import urllib3

# Upload file to
def upload_to(self, filename):
    name = "profile." + filename.split(".")[1]
    return "users/%s/%s" % (self.pk, name)

# Generate destination FileField filename arg to the following pattern: MEDIA_ROOT/<group_name>_<group_id>/filename
def generate_filename(self, filename):
    filename = urllib3.unquote(filename)
    return "groups/%s/%s" % (self.pk, filename)
