from django.urls import include, path
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import (TokenRefreshView)

from .views import CustomTokenObtainPairView


urlpatterns = [
    path('', get_schema_view()),
    path('auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('auth/token/', CustomTokenObtainPairView.as_view(), name='test_token'),
    path('auth/token/refresh/', TokenRefreshView.as_view()),    

    # User urls
    path('user/', include('user.urls')),
]
