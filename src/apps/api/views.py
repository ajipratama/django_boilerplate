from rest_framework import status
from rest_framework.response import Response
from rest_framework_simplejwt.exceptions import InvalidToken, TokenError
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView

from user.models import User


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = TokenObtainPairSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        try:
            serializer.is_valid(raise_exception=True)
            token_serializer = serializer.validated_data
            user_id = User.objects.get(email=request.data['email']).id
            token_serializer.update(user_id=user_id)
        except TokenError as e:
            raise InvalidToken(e.args[0])
        return Response(token_serializer, status=status.HTTP_200_OK)
